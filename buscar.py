
import requests
from bs4 import BeautifulSoup

pagina = "https://elkarteak.org/mapa.php"
'''
ambitos = {
    "todas": "Todas",
    "4": "Acción social",
    "1": "Cooperación al desarrollo",
    "20": "Cultura",
    "19": "Deporte",
    "18": "Desarrollo comunitario",
    "15": "Diversidad funcional",
    "13": "Ecología y Medio Ambiente",
    "11": "Educación y Formación",
    "16": "Infancia, Familia y Tercera Edad",
    "9": "Inmigración",
    "8": "Inserción Social/Atención a Colectivos",
    "2": "Juventud y Tiempo Libre",
    "17": "Mujer e Igualdad de género",
    "5": "Otras",
    "24": "Profesionales",
    "23": "Promoción del euskera",
    "22": "Protección animal",
    "10": "Socio-Sanitario",
    "21": "Vecinal"
}
'''
ambitos = [
    ('todas', 'Todas'), 
    ('4', 'Acción social'), 
    ('1', 'Cooperación al desarrollo'), 
    ('20', 'Cultura'), ('19', 'Deporte'), 
    ('18', 'Desarrollo comunitario'), 
    ('15', 'Diversidad funcional'), 
    ('13', 'Ecología y Medio Ambiente'), 
    ('11', 'Educación y Formación'), 
    ('16', 'Infancia, Familia y Tercera Edad'), 
    ('9', 'Inmigración'), 
    ('8', 'Inserción Social/Atención a Colectivos'), 
    ('2', 'Juventud y Tiempo Libre'), 
    ('17', 'Mujer e Igualdad de género'), 
    ('5', 'Otras'), 
    ('24', 'Profesionales'), 
    ('23', 'Promoción del euskera'), 
    ('22', 'Protección animal'), 
    ('10', 'Socio-Sanitario'), 
    ('21', 'Vecinal')]

def obtener_url(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')
    elkarteak = soup.find("div", {"id": "elkarteak_zerrenda"})
    links = elkarteak.find_all("h4")
    nombres_enlaces = []
    for link in links:
        href = link.find('a')['href']
        nombres_enlaces.append(href)
    return nombres_enlaces

def intro():
    cuenta = 0
    for i in range(len(ambitos)):
        print("[" + str(cuenta) + "]" + " - " + ambitos[i][1])
        cuenta += 1

    print("\n")
    print("Elige la categoría de búsqueda que deseas buscar:")
    entrada = int(input())
    cat = ambitos[entrada][0]
    catNom = ambitos[entrada][1]
    return cat, catNom

def limpiarDato(dato):
    if dato == "000000000" or "---" in dato:
        return ""
    else:
        return dato

def obtener_datos(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')
    elkarteak = soup.find("div", {"id": "contenido"})
    datos = []

    #Nombre
    nombre = elkarteak.find_all("h3")[0].text
    datos.append(["nombre", nombre])
    #Categoria
    div_campo_ficha_categoria = elkarteak.find("div", {"class": "campo-ficha aktibitatea"})
    if div_campo_ficha_categoria != None:
        categoria = div_campo_ficha_categoria.find_next_sibling("p").text
        datos.append(["categoria", "Asociación "+limpiarDato(categoria)])
    else:
        datos.append(["categoria", ""])
    #Direccion
    div_campo_ficha_direccion = elkarteak.find("div", {"class": "campo-ficha non"})
    if div_campo_ficha_direccion != None:
        direccion = div_campo_ficha_direccion.find_next_sibling("p").text
        datos.append(["direccion", limpiarDato(direccion)])
    else:
        datos.append(["direccion", ""])
    #Web
    div_campo_ficha_web = elkarteak.find("div", {"id": "texto-sociales"})
    web = div_campo_ficha_web.find("a", {"class": "externo webasoc"})
    if web != None:
        datos.append(["web", limpiarDato(web["href"])])
    else:
        datos.append(["web", ""])
    #Telefono
    div_campo_ficha_telefono = elkarteak.find("div", {"class": "campo-ficha telefono"})
    if div_campo_ficha_telefono != None:
        telefono = div_campo_ficha_telefono.find_next_sibling("p").text
        datos.append(["telefono", limpiarDato(telefono)])
    else:
        datos.append(["telefono", ""])
    #Descripcon
    elemento_anterior = elkarteak.find("p", {"class": "textos-ficha separadorT"})
    div_campo_ficha_descripcion = elemento_anterior.find_next_sibling("div")
    descripcion = div_campo_ficha_descripcion.get_text(strip=True)
    if descripcion != None:
        datos.append(["descripcion", limpiarDato(descripcion)])
    else:
        datos.append(["descripcion", ""])
    
    return datos

def generarFichero(nombre, datos):
    import csv
    import os.path as path
    import os
    fichero = nombre+'.csv'
    if path.exists(fichero):
        print("Eliminando el fichero anterior: "+fichero)
        os.remove(fichero) #Eliminamos anteriores

    print("Creando el fichero: "+fichero)
    with open(nombre+'.csv', 'w' , newline='') as file:
        writer = csv.writer(file, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(['nombre', 'categoria', 'direccion', 'web', 'telefono', 'descripcion'])
        for d in datos:
            writer.writerow([d[0][1], d[1][1], d[2][1], d[3][1], d[4][1], d[5][1]])

    if path.exists(fichero):
        print("Exportación exitosa: "+fichero)

cat, catNom = intro()

if cat != None:
    url = pagina+"?ambito="+cat+"&texto_busqueda_libre=&c=elkarteak&a=filtro"
    print("Buscando en el sitio: "+url)
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')

    enlaces = []
    enlaces = obtener_url(url)
    anteriores = soup.find("a", {"id": "aurrekoak"}, href=True)
    
    if anteriores != None:
        urAnt = anteriores['href']
        finAnteriores = False
        while finAnteriores == False:
            responseAnt = requests.get(pagina+urAnt)
            soupAnt = BeautifulSoup(responseAnt.content, 'html.parser')
            anter = soupAnt.find("a", {"id": "aurrekoak"}, href=True)
            for e in obtener_url(pagina+urAnt):
                enlaces.append(e)

            if anter != None:
                urAnt = anter['href']
            else:
                finAnteriores = True

    print("Se han encontrado "+str(len(enlaces))+" resultados")
    lista_datos = []
    for e in enlaces:
        datos = obtener_datos(pagina+e)
        lista_datos.append(datos)

    generarFichero(catNom, lista_datos)

else:
    print("No has seleccionado ninguna categoria")

